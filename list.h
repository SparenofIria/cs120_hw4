#ifndef LIST_H
#define LIST_H

#include <stdio.h>

struct course{
  int dept; //001 to 700
  int coursenum; //100 to 899
  short credA; //0 to 5
  short credB; //0 or 5
  char title[31]; //30 chars + null pointer 
  char gradeA;
  char gradeB;
};

typedef struct course Course;

struct node {
  Course data;
  struct node * next;
};

typedef struct node Node;

/*
 * Returns the size of the linked-list headed by *headptr
 * (size(NULL) is 0)
 */
int size(const Node* headptr);

/*
 * Each item in the list headed by *headptr is printed to outfile
 * followed by a single space (items are printed in the order found in
 * the list)
 */
void fprintList(FILE* outfile, const Node* headptr);

/*
 * Alternative recursive implementation of (fprintList)
 */
void fprintListR(FILE* outfile, const Node* headptr);

/*
 * In reverse order, each item in the list headed by *headptr is
 * printed to outfile followed by a single space
 */
void fprintListRev(FILE* outfile, const Node* headptr);

/*
 * specialized versions of the above functions that print to stdout
 */
void printList(const Node* headptr);
void printListR(const Node* headptr);
void printListRev(const Node* headptr);

/*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to include an additional node with the specificed data
 * followed by the elements of the original list
 */
void addFront(Node** headptrrf, Course data);

/*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to remove the first node if there is one; if there was
 * a node to remove, the function returns 1, if there was no node to
 * remove, the function returns 0
 */
int deleteFront(Node** headptrref);

/*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to remove the first node with dept and course equal to
 * 'depttodelete' and 'coursetodelete'; 1 is returned if such a node
 * was found and 0 otherwise
 */
int delete(Node** headptrref, int depttodelete, int coursetodelete);

/*
 * the list referenced by headptrrf (which points to a pointer to the
 * first node of some linked list or points to NULL for an empty list)
 * is modified to remove all nodes from the list
 */
void clearList(Node** headptrref);

/*
 * Returns a pointe to the first node with data matching tofind or
 * NULL if no such node is found
 */
Node* find(Node* headptr, int depttofind, int coursetofind);

/*
 * Finds the first node with data matching old and replaces it with
 * new; returns 1 if the replace was succesful and 0 if no matching
 * node was found
 */
int replace(Node* headptr, int oldd, int oldc, int newd, int newc);

#endif
