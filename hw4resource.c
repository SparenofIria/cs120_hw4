#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include "list.h"

int IsFileExisting(char* filename){//Based on SO 230062
  //printf("Debug: IsFileExisting, currfilename = %s\n", filename);
  FILE* file;
  file = fopen(filename, "r+b");
  if (file){//if exists
    fclose(file);//you can only fclose if it exists
    return 1;
  }
  return 0;
}

//Converts course number to database in index. 1-560000 inc
int CourseToIndex(int dept, int coursenum){
  return (dept-1)*800 + coursenum - 100 + 1;
}

//used in Options 1 and 2 - controls processing and error handling 
int ProcessLineToDatabase(char* line, FILE* database){
  //printf("Debug: line[0] is as follows: %c\n", line[0]);
  //printf("Notice: If above was blank, then the first char was a space.\nThis function DOES NOT take care of moving the pointer to deal with this.\n");
  char deptstr[3] = {line[3], line[4], line[5]};
  char coursestr[3] = {line[7], line[8], line[9]};

  //Errors in formatting
  if(line[2]!='.' || line[6]!='.' || line[12]!='.'){
    //printf("Debug: line[2], line[6], line[12]: %c, %c, %c\n", line[2], line[6], line[12]);
    perror("Error: Incorrect formatting - periods\n");
    return 0;
  }
  if(line[3]<'0'||line[3]>'7'||line[4]<'0'||line[4]>'9'||line[5]<'0'||line[5]>'9'||line[7]<'1'||line[7]>'8'||line[8]<'0'||line[8]>'9'||line[9]<'0'||line[9]>'9'){
    //printf("Debug: line[3], line[5], line[7], line[9]: %c, %c, %c, %c\n", line[3], line[5], line[7], line[9]);
    perror("Error: Incorrect formatting - Course/Dept number\n");
    return 0;
  }
  if(line[11]<'0'||line[11]>'5'||(line[13]!='0'&&line[13]!='5')){
    perror("Error: Incorrect formatting - Credits\n");
    return 0;
  }

  int coursenum = atoi(coursestr);
  int deptnum = atoi(deptstr);
  //printf("Debug: deptnum = %d, coursenum = %d\n", deptnum, coursenum);
  int databaseid = CourseToIndex(deptnum, coursenum);
  //printf("Debug: DatabaseID = %d\n", databaseid);
  //Add to database here, check for validity
  //Below is code meant to move pointer to the line in database
  fseek(database, (databaseid-1)*46, SEEK_SET);

  //Store content of entry at line
  char tempstore[15];//longer than needed simply for analysis purposes
  fread(tempstore, sizeof(char), 12, database);//first 7 chars is EN.000.
  //use this to compare with the existing entry for legitimacy
  //printf("Debug: tempstore: %s\n", tempstore);
  //printf("Debug: 3, 4, 5 of tempstore: %c %c %c\n", tempstore[3], tempstore[4], tempstore[5]);
  fseek(database, (databaseid-1)*46, SEEK_SET);//reset after use of fread

  if(tempstore[3]=='0'&&tempstore[4]=='0'&&tempstore[5]=='0'){//blank entry
    //Goal is to completely rewrite
    //line++;//increment pointer to remove mysterious space at start of line
    //ABOVE LINE NO LONGER USED BECAUSE IT ONLY HAPPENS WITH OPTION 2
    fwrite(line, sizeof(char), 45, database);//I pray that this works.
  }else{
    perror("Error: Course already exists - cannot override\n");
    return 0;
  }
  fseek(database, 0, SEEK_SET);//movepointer back to start to prevent weird things from happening.
  fflush(database);//Force push changes
  return 1;//All clear
}

//used in Options 3 and 4 - controls printing of courses
int PrintCourseInformation(FILE* database, int deptnum, int coursenum){
  int databaseid = CourseToIndex(deptnum, coursenum);
  fseek(database, (databaseid-1)*46, SEEK_SET);//move pointer
  char line[45];
  fread(line, sizeof(char), 45, database);
  //printf("Debug: PrintCourseInfo: Thing to print is currently as follows: %s\n", line);
  //printf("Debug: line[0, 1, 2, 3, 4, 5]: %c %c %c %c %c %c\n", line[0], line[`1], line[2], line[3], line[4], line[5]);

  if(line[3]<'1'&&line[4]<'1'&&line[5]<'1'){//BAD. Equals 0 (blank) or completely wrong cases (not numbers) tested here.
    return 0;//tried to read from blank or invalid thing
  }

  printf("%s\n", line);

  fseek(database, 0, SEEK_SET);//movepointer back to start to prevent weird things from happening.
  return 1;//All clear
}

//Used in Option 5 - replaces title of given course            
int ChangeLineToDatabase(char* line, FILE* database){
  //printf("Debug: line is as follows: %s\n", line);       
  char deptstr[4] = {line[0], line[1], line[2], '\0'};
  char coursestr[4] = {line[4], line[5], line[6], '\0'};

  //Errors in formatting                                                       
  if(line[3]!='.'){
    perror("Error: Incorrect formatting - periods\n");
    return 0;
  }
  if(line[0]<'0'||line[0]>'7'||line[1]<'0'||line[1]>'9'||line[2]<'0'||line[2]>'9'||line[4]<'1'||line[4]>'8'||line[5]<'0'||line[5]>'9'||line[6]<'0'||line[6]>'9'){
    perror("Error: Incorrect formatting - Course/Dept number\n");
    return 0;
  }

  int coursenum = atoi(coursestr);
  int deptnum = atoi(deptstr);
  //printf("Debug: deptnum = %d, coursenum = %d\n", deptnum, coursenum);        
  int databaseid = CourseToIndex(deptnum, coursenum);
  //printf("Debug: DatabaseID = %d\n", databaseid);                          
  //Add to database here, check for validity                                   
  //Below is code meant to move pointer to the start of lin in database        
  fseek(database, (databaseid-1)*46, SEEK_SET);

  //Store content of entry at line                                             
  char tempstore[15];//longer than needed simply for analysis purposes         
  fread(tempstore, sizeof(char), 12, database);//first 7 chars is EN.000.      
  //use this to compare with the existing entry for legitimacy                 
  fseek(database, (databaseid-1)*46, SEEK_SET);//reset after use of fread 

  if(tempstore[3]=='0'&&tempstore[4]=='0'&&tempstore[5]=='0'){//blank entry
    perror("Error: Course does not exist\n");    
    return 0;    
  }else{
    fseek(database, (databaseid-1)*46+15, SEEK_SET);//reset after use of fread
    fwrite("                              ", sizeof(char), 30, database);
    line+=8;//move line up 8 chars
    fseek(database, (databaseid-1)*46+15, SEEK_SET);//reset after use of fwrite
    //printf("Debug: strlen(line) = %i\n", strlen(line));
    fwrite(line, sizeof(char), strlen(line), database);
  }
  fseek(database, 0, SEEK_SET);//movepointer back to start to prevent weird things from happening.                                                            
  fflush(database);//Force push changes                                        
  return 1;//All clear 
}

//Used in Option 6 - purges course 
int PurgeCourseFromDatabase(FILE* database, int deptnum, int coursenum){
  int databaseid = CourseToIndex(deptnum, coursenum);
  fseek(database, (databaseid-1)*46, SEEK_SET);//move pointer
  char line[45]; //existing record
  fread(line, sizeof(char), 45, database);
  if(line[3]<'1'&&line[4]<'1'&&line[5]<'1'){//BAD.
    perror("Error: Trying to wipe nonexistent course\n");
    return 0;                     
  }
  fseek(database, (databaseid-1)*46, SEEK_SET);//move pointer
  fwrite("EN.000.000 0.0                               ", sizeof(char), 45, database);
  return 1;
}

//Used in Option 7 - adds course to semester                      
int ProcessCourseAdd(FILE* database, Node* semester, char* line){
  //printf("Debug: ProcessCourseAdd - line = %s\n", line);
  char deptstr[4] = {line[0], line[1], line[2], '\0'};
  char coursestr[4] = {line[4], line[5], line[6], '\0'};
  //printf("Debug: DisplayCourseDetails - deptstr, coursestr: %s %s\n", deptstr  , coursestr);                                                                
  int coursenum = (int) atoi(coursestr);
  int deptnum = (int) atoi(deptstr);
  //printf("Debug: DisplayCourseDetails - deptnum = %d, coursenum = %d\n"); 
  //char grade1 = line[8];//Needed later
  //char grade2 = line[9];//Needed later
  int databaseid = CourseToIndex(deptnum, coursenum);
  fseek(database, (databaseid-1)*46, SEEK_SET);

  char* tempstore = calloc(45, sizeof(char));//longer than needed simply for analysis purposes         
  fread(tempstore, sizeof(char), 45, database);//first 7 chars is EN.000.      
  //use this to compare with the existing entry for legitimacy                 
  fseek(database, (databaseid-1)*46, SEEK_SET);//reset after use of fread     
  //short cred1 = (short)(tempstore[11]-'0');//Needed later
  //short cred2 = (short)(tempstore[13]-'0');//Needed later
  //printf("Debug: Credits are %i.%i\n", cred1, cred2);

  tempstore+=15;
  
  if(semester==NULL){printf("Semester is Null.\n");}//Warning evasion

  if(tempstore[3]=='0'&&tempstore[4]=='0'&&tempstore[5]=='0'){//blank entry    
    perror("Error: Course does not exist\n");
    return 0;
  }else{
    //See if it is already there. If not, add.
    /*printf("Debug: Now creating new course node\n");
    Course newcourse = { .dept = deptnum, .coursenum = coursenum, .credA = cred1, .credB = cred2, .title = tempstore, .gradeA = grade1, .gradeB = grade2};
    Node* newnode = malloc(sizeof(Node));
    newnode->data = newcourse;
    newnode->next = NULL;
    printf("Debug: Now testing if node can be added\n");
    if(semester == NULL){
      printf("Semester Linked List in Empty. Adding first node.\n");
      semester = newnode;
    }else{//Have to search if it is there. If not, add
      printf("Debug: Now testing if course in semester\n");
      Node* pointer = semester;
      while(pointer->next!=NULL){
	printf("Debug: Test\n");
	if((pointer->data).dept == deptnum && (pointer->data).coursenum == coursenum){
	  perror("Error: Course already in semester\n");
	  return 0;
	}
	printf("Debug: Test2 - pointer moves\n");
	pointer = pointer->next;
	if(!pointer){break;}//null. May not function
      }
      //Course needs to be added
      printf("Now adding course into semester\n");
      Node* pointerpost = semester->next;
      Node* pointerpre = semester;
      while(pointerpost->next!=NULL){
	int cnumpost = (pointerpost->data).coursenum;
	int deptpost = (pointerpost->data).dept;
	int cnumpre = (pointerpre->data).coursenum;
	int deptpre = (pointerpre->data).dept;
	//Do marvelous things here
	if(coursenum<cnumpre){//if by some chance less than first item
	  if(deptnum<deptpre){
	    newnode->next = pointerpre;
	    semester = newnode;
	    return 1;
	  }
	}
	if(coursenum<cnumpost && coursenum>cnumpre){//between pre and post
	  if(deptnum<deptpost && deptnum>deptpre){
	    newnode->next = pointerpost;
	    pointerpre->next = newnode;
	    return 1;
	  }
	}
	pointerpost = pointerpost->next;
	pointerpre = pointerpre->next;
	}
      //OK, have exited while loop. IE, pointerpre is last node, pointerpost is null
    pointerpre->next = newnode;
    }*/
  }
  return 1;
}

//All functions return -1 for a failed transaction

/*Loads course data from input into the semester course node*/
/*void LoadCourseDataToStruct(FILE* input, Node** list){
  int c1, c2;
  char div[3], title[30+1];
  int dept, num;
  int count = 0;
  int c = ' ';
  fscanf(input, "%2s.%d.%d %d.%d ", div, &dept, &num, &c1, &c2);
  //load first 30 chars of title below
  while (count < 30 && (c = fgetc(input)) != '\n') {
    title[count++] = (char) c;
  }
  title[count] = '\0';
  while (c != '\n') c = fgetc(input);   // skip to end of line
  }*/

/*Create new courses from a plain text file, add to database. Get name
  from user. One line of info per course in format EN.ddd.nnn c.f
  title. Use cours no. to determine which record to store in. If
  course not in use, satisfied. Else invalid. Check all things for
  validity, process entire file.*/
int CreateNewCoursesFromFile(FILE* database){ //Option 1
  bool valid = true;
  printf("Please type in the filepath of the data file\n");
  char filepath[32];
  fscanf(stdin, "%s", filepath);
  if(IsFileExisting(filepath)==0){
    perror("Error: File does not exist\n");
    return -1;
  }
  FILE* temp = fopen(filepath, "r");//file exists, so open it
  char line[45];
  while(!feof(temp)){
    for(int i = 0; i < 15; i++){
      line[i] = fgetc(temp);//the first fgetc is always a space  
    }
    if(line[0]==EOF||line[1]==EOF){break;}//yeah, this is bad. Kill immediately.
    line[14] = ' ';
    int c = ' ';
    int count = 0;
    bool hitnewline = false;
    while(count<30&&(c = fgetc(temp))!= '\n'){
      line[15+count] = (char) c;
      count++;
    }
    if(count<30){hitnewline = true;}
    while(count<30){
      line[15+count] = ' ';
      count++;
    }

    //line[45] = '\0'; //would overwrite the new line...     
    //AIM IS TO REPLACE THE FIRST 45 CHARACTERS ONLY. DO NOT TOUCH THE NEW LINE
    //Another function removes the opening space, so max size 45    
    //printf("Debug: line is currently the following: %s\n", line);
    //line is now ready for processing.
    if(ProcessLineToDatabase(line, database)!=1){//Note: continue regardless of errors
      valid = false;
    }

    //If has not reached end of line, must do so now.
    if(!hitnewline){
      while((c = fgetc(temp))!= '\n'){}
    }
    
  }
  if(valid==false){return -1;}
  return 0;
}

//Same as #1 except one line of course info from stdin
int AddSingleCourseToDatabase(FILE* database){ //Option 2
  bool valid = true;
  char* line = calloc(sizeof(char), 45);//any more will cause reading problems
  for(int i = 0; i < 16; i++){
    line[i] = fgetc(stdin);//the first fgetc is always a space
  }
  line[15] = ' ';
  int c = ' ';
  int count = 0;
  while(count<30&&(c = fgetc(stdin))!= '\n'){
    line[16+count] = (char) c;
    count++;
  }
  while(count<30){
    line[16+count] = ' ';
    count++;
  }
  //line[45] = '\0'; //would overwrite the new line...
  //AIM IS TO REPLACE THE FIRST 45 CHARACTERS ONLY. DO NOT TOUCH THE NEW LINE
  //printf("Debug: line is currently the following: %s\n", line);
  //line is now ready for processing.
  line++;//Remove blank space before processing
  if(ProcessLineToDatabase(line, database)!=1){//Note: continue regardless of errors
    valid = false;
  }

  //free(line);//Causes hideous dump of errors

  if(valid==false){return -1;}
  return 0;
}


/*Display all details of particualr course in format EN.ddd.nnn c.f
  title. Get Userinput for course number ddd.nnn. If input error or
  DNE, failed, else display details*/
int DisplayCourseDetails(FILE* database){ //Option 3
  bool valid = true;
  //Obtain user information here
  char* line = calloc(sizeof(char), 8);//any more will cause reading problems
  for(int i = 0; i < 8; i++){
    line[i] = fgetc(stdin);//the first fgetc is always a space
  }

  //printf("Debug: line[0] is %c\n", line[0]);
  line++;//remove blank space at front
  //printf("Debug: line[0, 1, 2, 3, 4, 5]: %c %c %c %c %c %c\n", line[0], line[1], line[2], line[3], line[4], line[5]);

  if(line[0]<'0'||line[0]>'7'||line[1]<'0'||line[1]>'9'||line[2]<'0'||line[2]>'9'||line[4]<'1'||line[4]>'8'||line[5]<'0'||line[5]>'9'||line[6]<'0'||line[6]>'9'){
    perror("Error: Incorrect Course Format - Number\n");
  }
  if(line[3]!='.'){
    perror("Error: Incorrect Course Format - Period\n");
  }

  //Error handling for bad stuff here
  char deptstr[4] = {line[0], line[1], line[2], '\0'};
  char coursestr[4] = {line[4], line[5], line[6], '\0'};
  //printf("Debug: DisplayCourseDetails - deptstr, coursestr: %s %s\n", deptstr, coursestr);
  int coursenum = (int) atoi(coursestr);
  int deptnum = (int) atoi(deptstr);
  //printf("Debug: DisplayCourseDetails - deptnum = %d, coursenum = %d\n");

  //call print function
  if(PrintCourseInformation(database, deptnum, coursenum)==0){
    valid = false;
    perror("Error: Course not in Database - blank course\n");
  }

  if(valid==false){return -1;}
  return 0;
}

//Display all non-blank (actual) courses. Cannot fail.
int DisplayAllCourses(FILE* database){ //Option 4
  printf("Now printing every course in database\n");
  char* line = calloc(sizeof(char), 45); //reading from database
  
  fseek(database, 0, SEEK_SET);//move pointer
  
  for(int i = 0; i < 560000; i++){
    fseek(database, i*46, SEEK_SET);
    fread(line, sizeof(char), 45, database);
    //printf("Debug: DisplayAllCourses: This is the read line post pointer: %s\n", line);
    char deptstr[4] = {line[3], line[4], line[5], '\0'};
    char coursestr[4] = {line[7], line[8], line[9], '\0'};
    //printf("Debug: DisplayAllCourses - deptstr, coursestr: %s %s\n", deptstr, coursestr);
    //printf("Debug: line 3, 4, 5, 7, 8, 9: [%c][%c][%c].[%c][%c][%c]\n", line[3], line[4], line[5], line[7], line[8], line[9]);

    int coursenum = atoi(coursestr);
    int deptnum = atoi(deptstr);
    //printf("Debug: DisplayAllCourses - deptnum = %d, coursenum = %d\n");

    //So, I have some error cases that seem to be problematic. Solution below
    if(line[3]=='0'&&line[4]=='0'&&line[5]=='0'){//blank
    }else{
      //call print function, which handles error handling
      PrintCourseInformation(database, deptnum, coursenum);
    }  
  }
  return 0;
}

/*Change course name. Get course number ddd.nnn and new name from
  input. If course DNE or input error, invalid.*/
int ChangeCourseName(FILE* database){ //Option 5
  bool valid = true;
  char* line = calloc(sizeof(char), 45);//any more will cause reading problems
  for(int i = 0; i < 9; i++){
    line[i] = fgetc(stdin);//the first fgetc is always a space   
  }
  line[8] = ' ';
  int c = ' ';
  int count = 0;
  while(count<30&&(c = fgetc(stdin))!= '\n'){
    line[9+count] = (char) c;
    count++;
  }
  while(count<30){
    line[9+count] = ' ';
    count++;
  }

  line++;
  if(ChangeLineToDatabase(line, database)!=1){
    valid = false;
  }

  //free(line);

  if(valid==false){return -1;}
  return 0;
}

/*Delete course and replace with blank. Get course num ddd.nnn. If DNE
  or input error, failed.*/
int DeleteCourse(FILE* database){ //Option 6
  bool valid = true;
  char* line = calloc(sizeof(char), 8);
  for(int i = 0; i < 8; i++){
    line[i] = fgetc(stdin);//the first fgetc is always a space              
  }
  line++;
  //printf("Debug: DeleteCourse - line is currently the following: %s\n", line);

  if(line[0]<'0'||line[0]>'7'||line[1]<'0'||line[1]>'9'||line[2]<'0'||line[2]>'9'||line[4]<'1'||line[4]>'8'||line[5]<'0'||line[5]>'9'||line[6]<'0'||line[6]>'9'){
    perror("Error: DeleteCourse -  Incorrect Course Format - Number\n");
    valid = false;
  }
  if(line[3]!='.'){
    perror("Error: DeleteCourse - Incorrect Course Format - Period\n");
    valid = false;
  }

  char deptstr[4] = {line[0], line[1], line[2], '\0'};
  char coursestr[4] = {line[4], line[5], line[6], '\0'};
  //printf("Debug: DisplayCourseDetails - deptstr, coursestr: %s %s\n", deptstr  , coursestr); 
  int coursenum = (int) atoi(coursestr);
  int deptnum = (int) atoi(deptstr);

  if(PurgeCourseFromDatabase(database, deptnum, coursenum)!=1){
    valid = false;
  }

  //free(line);  

  if(valid==false){return -1;}
  return 0;
}

/*Add course to semester course listing. Req. semester (0-9), course
  no. ddd.nnn, and 2 char letter grade. See
  ugrad.cs.jhu.edu/~cs120/assign/hw4.shtml for details. If valid,
  insert course in correct place (sorted list).*/
int AddCourseToSemester(FILE* database, Node** semesters){ //Option 7;
  bool valid = true;
  char seminput[2];
  fscanf(stdin, seminput);
  seminput[1]='\0';
  if(seminput[0]<'0'||seminput[0]>'9'){
    perror("Error: Invalid Semester\n");
    return -1;
  }
  int semester = atoi(seminput);

  char* line = calloc(sizeof(char), 11);//Must include course num and grade
  for(int i = 0; i < 11; i++){
    line[i] = fgetc(stdin);//the first fgetc is always a space              
  }
  line++;
  //printf("Debug: line is currently the following: %s\n", line);

  if(line[0]<'0'||line[0]>'7'||line[1]<'0'||line[1]>'9'||line[2]<'0'||line[2]>'9'||line[4]<'1'||line[4]>'8'||line[5]<'0'||line[5]>'9'||line[6]<'0'||line[6]>'9'){
    perror("Error: Incorrect Course Format - Number\n");
    valid = false;
  }
  if(line[3]!='.'){
    perror("Error: Incorrect Course Format - Period\n");
    valid = false;
  }

  line[8] = toupper(line[8]);
  //Expects grade in form A+, B-, C/
  if(line[8]<'A'||line[8]>'Z'){
    perror("Error: Illegitimate Grade\n");
    valid = false;
  }
  if(line[8]>'F'&&line[8]!='I'&&line[8]!='S'&&line[8]!='U'){
    perror("Error: Illegitimate Grade\n");
    valid = false;
  }
  if(line[9]!='/'&&line[9]!='+'&&line[9]!='-'){
    perror("Error: Illegitimate Grade\n");
    valid = false;
  }

  if(valid){ProcessCourseAdd(database, semesters[semester], line);}

  if(valid==false){return -1;}
  return 0;
}

/*Remove course from semester course listing. Req semester and course
  number. If invalid semester or course, failed transaction.*/
int RemoveCourseFromSemester(Node** semesters){ //Option 8
  bool valid = true;
  char seminput[2];
  fscanf(stdin, seminput);
  seminput[1]='\0';
  if(seminput[0]<'0'||seminput[0]>'9'){
    perror("Error: Invalid Semester\n");
    return -1;
  }
  //int semester = atoi(seminput);

  char* line = calloc(sizeof(char), 11);//Must include course num and grade
  for(int i = 0; i < 11; i++){
    line[i] = fgetc(stdin);//the first fgetc is always a space              
  }
  line++;
  printf("Debug: line is currently the following: %s\n", line);

  if(line[0]<'0'||line[0]>'7'||line[1]<'0'||line[1]>'9'||line[2]<'0'||line[2]>'9'||line[4]<'1'||line[4]>'8'||line[5]<'0'||line[5]>'9'||line[6]<'0'||line[6]>'9'){
    perror("Error: Incorrect Course Format - Number\n");
    valid = false;
  }
  if(line[3]!='.'){
    perror("Error: Incorrect Course Format - Period\n");
    valid = false;
  }

  //NOT IMPLEMENTED
  

  if(valid==false){return -1;}
  return 0;
}

/*Display a semester course listing on screen in course number order,
  AKA sorted. Req. user input (sem, 0-9). If empty, output No courses
  this semester, else display in standard form*/
int DisplaySemesterCourseListing(Node** semesters){ //Option 9
  bool valid = true;
  char seminput[2];
  fscanf(stdin, seminput);
  seminput[1]='\0';
  if(seminput[0]<'0'||seminput[0]>'9'){
    perror("Error: Invalid Semester\n");
    return -1;
  }
  int semester = atoi(seminput);
  if(size(semesters[semester])==0){printf("No courses this semester\n");}
  
  printList(semesters[semester]);
  
  if(valid==false){return -1;}
  return 0;
}

/*Display merged ordered listing of all courses in all
  semesters. Remove repeats. See Option 9*/
/*int DisplaySemesterCourseListingAll(Node** semesters){ //Option 10, EC
  bool valid = true;

  if(valid==false){return -1;}
  return 0;
}*/

/*Compute and Display GPA for a particular semester in same way as
  original GPA example. If invalid semester or semester w/o courses,
  invalid*/ 
/*int ComputeSemesterGPA(Node** semesters){ //Option 11, EC
  bool valid = true;

  if(valid==false){return -1;}
  return 0;
  }*/
