/* Homework 4, 600.120 Spring 2015
Andrew Fan (afan5)
Section 2
Part 1 due March 6, 2015 
afan8878@gmail.com     
718-801-7404                         
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "list.h"
#include "hw4resource.h"

// NOTE: this file should not contain any functions other than main();

int main(int argc, char* argv[]){
  if (argc == 1) {
    printf("Usage: ./hw4 database.dat\n");
    return 1;  // exit program
  }

  //Obtain filepath of database (.dat)
  //If file does not exist, create it with 560,000 blanks.
  char filepath[32];
  strncpy(filepath, argv[1], 32);

  int legit = IsFileExisting(filepath);

  if(legit==0){//test was null
    FILE* blankfile = fopen(filepath, "w+b");//open the file for stuffs
    printf("Database is empty: Now creating database\n");
    //printing hell to the file
    for(int i = 0; i<560000; i++){//46 chars per line
      fprintf(blankfile, "EN.000.000 0.0                               \n");
    }
    fclose(blankfile);
  }

  FILE* inputfile = fopen(filepath, "r+b");//open the file for stuffs

  //Initialize 10 semester linked lists in an array (IE an array of
  //pointers to a Node. Each Linked List contains a course per node,
  //one list per semester
  Node** semesterarray = calloc(10, sizeof(Node));
  for(int i = 0; i<10; i++){
    Node* newnode = malloc(sizeof(Node));
    semesterarray[i]=newnode;
  }

  //Interactive Menu
  bool satisfied = false;
  while(!satisfied){
    printf("Main: Please type your command\n");
    int userinput;
    fscanf(stdin, "%d", &userinput);
    if(userinput==0){//quit
      satisfied = true;
      printf("\nThank you for using this program\n");
    }else if(userinput==1){
      CreateNewCoursesFromFile(inputfile);
    }else if(userinput==2){
      AddSingleCourseToDatabase(inputfile);
    }else if(userinput==3){
      DisplayCourseDetails(inputfile);
    }else if(userinput==4){
      DisplayAllCourses(inputfile);
    }else if(userinput==5){
      ChangeCourseName(inputfile);
    }else if(userinput==6){
      DeleteCourse(inputfile);
    }else if(userinput==7){
      AddCourseToSemester(inputfile, semesterarray);
    }else if(userinput==8){
      RemoveCourseFromSemester(semesterarray);
    }else if(userinput==9){
      DisplaySemesterCourseListing(semesterarray);
    }else if(userinput==10){
      printf("This function has not been implemented\n");
    }else if(userinput==11){
      printf("This function has not been implemented\n");
    }else{
      printf("Acceptable numbers range from 0 through 11.\n");
    }

  }

  fclose(inputfile);

  for(int i = 0; i<10; i++){
    free(semesterarray[i]);
  }
  free(semesterarray);

  return 0;
}
