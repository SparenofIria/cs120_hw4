#include "list.h"
#include <stdio.h>
#include <stdlib.h>

int size(const Node* headptr) {
  if (headptr) {
    return 1 + size(headptr->next);
  } else {
    return 0;
  }
}

void fprintList(FILE* outfile, const Node* headptr) {
  for (; headptr; headptr = headptr->next) {
    fprintf(outfile, "EN.%d.%d %i.%i %s ", (headptr->data).dept, (headptr->data).coursenum, (headptr->data).credA, (headptr->data).credB, (headptr->data).title);
  }
  fprintf(outfile, "\n");
}

void fprintListR(FILE* outfile, const Node* headptr) {
  if (headptr) {
    fprintf(outfile, "%d.%d ", (headptr->data).dept, (headptr->data).coursenum);
    fprintList(outfile, headptr->next);
  }
  fprintf(outfile, "\n");
}

void fprintListRev(FILE* outfile, const Node* headptr) {
  if (headptr) {
    fprintListRev(outfile, headptr->next);
    fprintf(outfile, "%d.%d ", (headptr->data).dept, (headptr->data).coursenum);
  }
  fprintf(outfile, "\n");
}

void printList(const Node* headptr) {
  fprintList(stdout, headptr);
}

void printListR(const Node* headptr) {
  fprintListR(stdout, headptr);
}

void printListRev(const Node* headptr) {
  fprintListRev(stdout, headptr);
}

void addFront(Node** headptrrf, Course data) {
  Node* oldheadptr = *headptrrf;          //-to keep the *s straight
  Node* newheadptr = malloc(sizeof(Node));//-get a new node
  newheadptr->data = data;                //-populate with data
  newheadptr->next = oldheadptr;          //-point next to old head
  *headptrrf = newheadptr;                //-update the head by ref
}

int deleteFront(Node** headptrref) {
  Node* oldheadptr = *headptrref;
  if (oldheadptr) {
    *headptrref = oldheadptr->next;
    free(oldheadptr);
    return 1;
  } else {
    return 0;
  }
}

int delete(Node** headptrref, int depttodelete, int coursetodelete) {
    for (; *headptrref; headptrref = &((*headptrref)->next)) {
      //printf("Debug: depttodelete = %d, coursetodelete = %d\n", depttodelete, coursetodelete);
      //printf("Debug: pointer dept = %d, pointer course = %d\n", ((*headptrref)->data).dept, ((*headptrref)->data).coursenum);
      if (((*headptrref)->data).dept == depttodelete && ((*headptrref)->data).coursenum == coursetodelete) {
            return deleteFront(headptrref);
        }
    }
    return 0;
}

void clearList(Node** headptrref) {
    Node* oldheadptr = *headptrref;
    if (oldheadptr) {                // if the list isn't empty
        clearList(&oldheadptr->next);// clear the rest of the list
        free(oldheadptr);            // free the remaining first node
        *headptrref = NULL;          // make the headptr point to NULL
    }
}


// // Alternative version of clearList that uses deleteFront
/*void clearList(Node** headptrref) {
    while (*headptrref) {
        deleteFront(headptrref);
    }
    }*/

Node* find(Node* headptr, int dtofind, int ctofind) {
  if (headptr == NULL || ((headptr->data).dept == dtofind && (headptr->data).coursenum == ctofind)) {
    return headptr;
  } else {
    return find(headptr->next, dtofind, ctofind);
  }
}


int replace(Node* headptr, int olddept, int oldcourse, int newdept, int newcourse) {
  Node* toChange = find(headptr, olddept, oldcourse);
  if (toChange) {
    (toChange->data).dept = newdept;
    (toChange->data).coursenum = newcourse;
    return 1;
  } else {
    return 0;
  }
}

