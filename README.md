## Homework 4 ##

By Andrew Fan, Intermediate Programming - CS120 Section 2

HOW TO COMPILE:

make

HOW TO RUN:

./hw4 database.dat

NOTES:
-use 'make clean' to remove many of the test files
-Option 5 works fine in the test file but duplicates credits in actual program
-Options 7-9 were unable to be completed due to time constraints. Their code has been adjusted to suppress warnings. However, since I did not even reach Option 8, it still brings up a warning regardless since I never implemented anything. This should be the ONLY warning

DEVEL STATUS:
Option 1: Done - Cleanup!
Option 2: Done - Cleanup!
Option 3: Done - Cleanup!
Option 4: Done - Cleanup!
Option 5: Done - Cleanup!
Option 6: Done - Cleanup!
Option 7: Implementing
Option 8: Not Implemented
Option 9: Implemented but not tested due to failure to implement Op. 7
Option 10: Not Implemented
Option 11: Not Implemented