# USAGE:
#
# // to compile:
# make
#
# // to compile tests and run tests:
# make test
#
# // remove compilation output files:
# make clean

# make variables let us avoid pasting these options in multiple places
CC = gcc 
CFLAGS = -std=c99 -Wall -Wextra -pedantic -O         # for final build
# CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0 -g   # for debugging

#Based off of HW3's makefile
#-c compiles but does not link.

bin: hw4 test

test: test_hw4funcs
	echo "Running tests..."
	./test_hw4funcs
	echo "All Tests Passed."

hw4resource.o: hw4resource.c hw4resource.h 
	$(CC) $(CFLAGS) -c hw4resource.c 

list.o: list.c list.h
	$(CC) $(CFLAGS) -c list.c

test_hw4funcs.o: test_hw4funcs.c hw4resource.h list.h
	$(CC) $(CFLAGS) -c test_hw4funcs.c

hw4.o: hw4.c hw4resource.h list.h
	$(CC) $(CFLAGS) -c hw4.c

test_hw4funcs: test_hw4funcs.o hw4resource.o list.o
	$(CC) $(CFLAGS) -o test_hw4funcs test_hw4funcs.o hw4resource.o list.o

hw4: hw4.o hw4resource.o list.o
	$(CC) $(CFLAGS) -o hw4 hw4.o hw4resource.o list.o

clean:
	rm -f *.o hw4 *# *~ test_hw4funcs database.dat testdatabase.dat
