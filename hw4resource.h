//All functions return -1 for a failed transaction

int IsFileExisting(char* input);

//converts course number to index in database
int CourseToIndex(int dept, int coursename);

//used in Options 1 and 2 - controls processing and error handling
int ProcessLineToDatabase(char* line, FILE* database);

//used in Options 3 and 4 - controls printing of course
int PrintCourseInformation(FILE* database, int deptnum, int coursenum);

//Used in Option 5 - replaces title of given course
int ChangeLineToDatabase(char* line, FILE* database);

//Used in Option 6 - purges course
int PurgeCourseFromDatabase(FILE* database, int deptnum, int coursenum);

//Used in Option 7 - adds course to semester
int ProcessCourseAdd(FILE* database, Node* semester, char* line);

/*Loads course data from input into the semester course node*/
void LoadCourseDataToStruct(FILE* input, Node** list);

/*Create new courses from a plain text file, add to database. Get name
  from user. One line of info per course in format EN.ddd.nnn c.f
  title. Use cours no. to determine which record to store in. If
  course not in use, satisfied. Else invalid. Check all things for
  validity, process entire file.*/
int CreateNewCoursesFromFile(FILE* database); //Option 1

//Same as #1 except one line of course info from stdin
int AddSingleCourseToDatabase(FILE* database); //Option 2

/*Display all details of particualr course in format EN.ddd.nnn c.f
  title. Get Userinput for course number ddd.nnn. If input error or
  DNE, failed, else display details*/
int DisplayCourseDetails(FILE* database); //Option 3

//Display all non-blank (actual) courses. Cannot fail.
int DisplayAllCourses(FILE* database); //Option 4

/*Change course name. Get course number ddd.nnn and new name from
  input. If course DNE or input error, invalid.*/
int ChangeCourseName(FILE* database); //Option 5

/*Delete course and replace with blank. Get course num ddd.nnn. If DNE
  or input error, failed.*/
int DeleteCourse(FILE* database); //Option 6

/*Add course to semester course listing. Req. semester (0-9), course
  no. ddd.nnn, and 2 char letter grade. See
  ugrad.cs.jhu.edu/~cs120/assign/hw4.shtml for details. If valid,
  insert course in correct place (sorted list).*/
int AddCourseToSemester(FILE* database, Node** semesters); //Option 7;

/*Remove course from semester course listing. Req semester and course
  number. If invalid semester or course, failed transaction.*/
int RemoveCourseFromSemester(Node** semesters); //Option 8

/*Display a semester course listing on screen in course number order,
  AKA sorted. Req. user input (sem, 0-9). If empty, output No courses
  this semester, else display in standard form*/
int DisplaySemesterCourseListing(Node** semesters); //Option 9

/*Display merged ordered listing of all courses in all
  semesters. Remove repeats. See Option 9*/
int DisplaySemesterCourseListingAll(Node** semesters); //Option 10, EC

/*Compute and Display GPA for a particular semester in same way as
  original GPA example. If invalid semester or semester w/o courses,
  invalid*/ 
int ComputeSemesterGPA(Node** semesters); //Option 11, EC
