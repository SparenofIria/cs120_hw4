/* Homework 4, 600.120 Spring 2015
Andrew Fan (afan5)       
Section 2                
Part 1 due March 6, 2015 
afan8878@gmail.com
718-801-7404
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include "list.h"
#include "hw4resource.h"

bool fileeq(char lhsName[], char rhsName[]);//declaring for no reason

void testLinkedList(void){
  Course course1 = {.dept = 100, .coursenum = 300, .credA = 2, .credB = 5, .title = "Blah1\0"};
  Course course2 = {.dept = 200, .coursenum = 320, .credA = 2, .credB = 5, .title = "Blah2\0"};
  Course course3 = {.dept = 300, .coursenum = 330, .credA = 2, .credB = 5, .title = "Blah3\0"};
  Course course4 = {.dept = 400, .coursenum = 350, .credA = 2, .credB = 5, .title = "Blah4\0"};
  Course course5 = {.dept = 500, .coursenum = 360, .credA = 2, .credB = 5, .title = "Blah5\0"};
  printf("Part 1: Creating the Linked List\n");
  Node headnode = {.data = course1, .next = NULL};
  Node* headptr = &headnode;
  printf("Part 2: Testing addFront\n");
  addFront(&headptr, course2);
  printf("Part 3: Testing size\n");
  assert(size(headptr)==2);
  printf("Part 4: Testing printList\n");
  printList(headptr);
  printf("Above should be 200.320, 100.300\n");
  printf("Part 5: Testing find\n");
  addFront(&headptr, course3);
  addFront(&headptr, course4);
  addFront(&headptr, course5);
  assert(find(headptr, 400, 350)!=NULL);
  assert(find(headptr, 600, 222)==NULL);
  printf("Part 6: Testing delete\n");
  printf("Current List: \n");
  printList(headptr);
  delete(&headptr, 400, 350);
  printf("Should now be: 500.360, 300.330, 200.320, 100.300\n");
  printList(headptr);
  printf("Part 7: Testing replace\n");
  assert(replace(headptr, 500, 360, 444, 666));
  printList(headptr);
  //clearList cannot be tested because things weren't defined with malloc
}

void testCourseToIndex(void){
  for(int i = 0; i < 10; i++){
    printf("Debug: CourseToIndex: %d\n", CourseToIndex(1, 100+i));
    assert(CourseToIndex(1, 100+i)==i+1);
  }
}

FILE* testCreateFile(void){
  printf("Debug: If this is not printing, something is horribly wrong.\n");
  char filepath[32] = "testdatabase.dat";
  int legit = IsFileExisting(filepath);
  printf("Debug: legit = %d\n", legit);
  if(legit==0){//test was null                                               
    FILE* blankfile = fopen(filepath, "w+b");//open the file for stuffs 
    printf("Database is empty: Now creating database\n");
    //printing hell to the file                             
    for(int i = 0; i<560000; i++){//46 chars per line                
      fprintf(blankfile, "EN.000.000 0.0                               \n");
    }
    fclose(blankfile);
  }

  FILE* returnfile = fopen(filepath, "r+b");
  assert(returnfile!=NULL);
  return returnfile;
}

void testSeekRead(FILE* database){
  for(int i = 0; i<5; i++){
    int databaseid = CourseToIndex(001, 100+i);
    printf("Current Database ID: %d\n", databaseid);
    fseek(database, (databaseid-1)*46, SEEK_SET);//move pointer
    char line[48];
    fread(line, sizeof(char), 47, database);
    printf("Below should print 46 chars and the first of next line, E\n");
    printf("Current line (47 chars): %s\n", line);
  }
}

void testPLtD(FILE* database){
  printf("testPLtD: Testing generic functionality of ProcessLineToDatabase\n");
  ProcessLineToDatabase("EN.001.100 1.0 First course                ", database);
  ProcessLineToDatabase("EN.001.101 1.5 Second course                ", database);
  ProcessLineToDatabase("EN.001.102 2.0 Third course                ", database);
  ProcessLineToDatabase("EN.001.105 2.5 Testing title longer than 30 chars", database);
  ProcessLineToDatabase("EN.001.110 1.0 Course 1 for Semester Course Lists", database);
  ProcessLineToDatabase("EN.001.111 1.5 Course 2 for Semester Course Lists", database);
  ProcessLineToDatabase("EN.001.112 2.0 Course 3 for Semester Course Lists", database);
  ProcessLineToDatabase("EN.001.113 2.5 Course 4 for Semester Course Lists", database);
  ProcessLineToDatabase("EN.001.114 3.0 Course 5 for Semester Course Lists", database);
}

void testPCI(FILE* database){
  printf("testPCI: Testing generic functionality of PrintCourseInformation\n");
  PrintCourseInformation(database, 1, 100);
  PrintCourseInformation(database, 1, 101);
  PrintCourseInformation(database, 1, 102);
  PrintCourseInformation(database, 1, 105);
}

void testCLtD(FILE* database){
  printf("testCLtD: Testing generic functionality of ChangeLineToDatabase\n");
  PrintCourseInformation(database, 1, 100);
  ChangeLineToDatabase("001.100 This title has been changed   ", database);
  PrintCourseInformation(database, 1, 100);
}

void testPCfD(FILE* database){
  printf("testPCfD: Testing generic functionality of PurgeCourseFromDatabase\n");
  PurgeCourseFromDatabase(database, 1, 102);
  printf("If looking at database, course 001.102 should be purged\n");
  PrintCourseInformation(database, 1, 102);
}

void testPCA(FILE* database, Node** semesterarray){
  printf("testPCA: Testing generic functionality of ProcessCourseAdd\n");
  ProcessCourseAdd(database, semesterarray[0], "001.110 A-");
  ProcessCourseAdd(database, semesterarray[0], "001.114 C+");
  ProcessCourseAdd(database, semesterarray[0], "001.111 B-");
  ProcessCourseAdd(database, semesterarray[0], "001.113 D+");
  ProcessCourseAdd(database, semesterarray[0], "001.112 A/");
}

int main(void) {
  printf("Testing Linked List\n");
  testLinkedList();
  printf("Linked List tests succeeded\n");
  printf("Testing CourseToIndex\n");
  testCourseToIndex();
  printf("Course to Index Initial quality check passed\n");

  printf("Testing IsFileExisting\n");
  FILE* database = testCreateFile();//if you leave out (), error
  printf("Have now written 560000 blanks\n");
  fseek(database, 0, SEEK_SET);//move to start
  printf("Now testing prints of existing data\n");
  printf("This tests the validity of my seek/read\n");
  testSeekRead(database);
  fseek(database, 0, SEEK_SET);
  printf("Now testing ProcessLineToDatabase\n");
  testPLtD(database);
  printf("Now testing ProntCourseInformation\n");
  testPCI(database);
  printf("Now testing ChangeLineToDatabase\n");
  testCLtD(database);
  printf("Now testing PurgeCourseFromDatabase\n");
  testPCfD(database);

  Node** semesterarray = calloc(10, sizeof(Node));
  for(int i = 0; i<10; i++){
    Node* newnode = malloc(sizeof(Node));
    semesterarray[i]=newnode;
  }
  printf("Now testing ProcessCourseAdd\n");
  testPCA(database, semesterarray);
}

//Below is code from hw3's test. May be useful, may not be useful. idk.

bool fileeq(char lhsName[], char rhsName[]) {
  FILE* lhs = fopen(lhsName, "r");
  FILE* rhs = fopen(rhsName, "r");

  // don't compare if we can't open the files
  if (!lhs || !rhs) return false;

  bool match = true;
  // read until both of the files are done or there is a mismatch
  while (!feof(lhs) || !feof(rhs)) {
    if (feof(lhs) ||                  // lhs done first
	feof(rhs) ||                  // rhs done first
	(fgetc(lhs) != fgetc(rhs))) { // chars don't match
      match = false;
      break;
    }
  }
  fclose(lhs);
  fclose(rhs);
  return match;
}
